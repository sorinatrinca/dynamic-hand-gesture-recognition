#include "HandDetector.h"

void HandDetector::init(int hLowValue, int hHighValue, int sLowValue, int sHighValue)
{
	hLow = hLowValue;
	hHigh = hHighValue;
	sLow = sLowValue;
	sHigh = sHighValue;
}

Mat HandDetector::detect(Mat frame)
{
	Mat hsvFrame, thresholdFrame;

	// Convert rbg frame to hsv
	cvtColor(frame, hsvFrame, COLOR_BGR2HSV);

	// Filter based on blue glove
	inRange(hsvFrame, Scalar(104, 72, 0), Scalar(152, 255, 255), thresholdFrame);

	return thresholdFrame;
}
