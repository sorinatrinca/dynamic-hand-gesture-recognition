#include "BackgroundRemover.h"

BackgroundRemover::BackgroundRemover()
{
	backgroundSubtractor = createBackgroundSubtractorMOG2(0,50);

	background = Mat(X_SIZE, Y_SIZE, CV_8UC3);
	foreground = Mat(X_SIZE, Y_SIZE, CV_8UC3);
}

void BackgroundRemover::calibrate(Mat frame)
{
	backgroundSubtractor->apply(frame, foregroundMask, 0.5);
}

void BackgroundRemover::filterImage(Mat& img)
{
	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(3, 3));
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(3, 3));

	erode(img, img, erodeElement);
	dilate(img, img, dilateElement);
	dilate(img, img, dilateElement);
}

Mat BackgroundRemover::getForeground(Mat frame)
{
	backgroundSubtractor->apply(frame, foregroundMask, 0.0);
	
	medianBlur(foregroundMask, foregroundMask, 5);
	filterImage(foregroundMask);

	for (int i = 0; i < foregroundMask.rows; i++) {
		for(int j = 0; j < foregroundMask.cols;j++)
		{
			if (foregroundMask.at<uchar>(i, j) != 255) {
				foregroundMask.at<uchar>(i, j) = 0;
			}
		}

	}

	medianBlur(foregroundMask, foregroundMask, 5);
	filterImage(foregroundMask);

	foreground = Scalar::all(0);
	frame.copyTo(foreground, foregroundMask);

	return foreground;
}
