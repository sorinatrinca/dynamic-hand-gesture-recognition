#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>


using namespace cv;
using namespace std;

class HandDetector {
private:
	int hLow, hHigh;
	int sLow, sHigh;
	int vLow, vHigh;

public:
	void init(int hLowValue, int hHighValue, int sLowValue, int sHighValue);
	Mat detect(Mat frame);

};
