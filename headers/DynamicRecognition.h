#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <fstream>
#include "Utils.h"

using namespace std;
using namespace cv;

class DynamicRecognition {
private:
	Mat directionTemplates;
	Mat staticGesturesTemplates;
	vector<string> labels;

public:
	void init();
	void endCurrentGesture();
	string recognize(Mat& frame, vector<double> relativePoints, vector<double> staticGestures);
};
