#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

enum Mode { writeMode, settingMode };
enum ThicknessMode { fine = 3, medium = 7, bold = 10 };
enum Color {black, red, blue, green, magenta};

using namespace cv;
using namespace std;

class Board {
private:
	ThicknessMode penThickness;
	Scalar penColor;
	Color currentColor;
	Point2d cursor;
	Mode userMode;
	Mat window;
	Mat auxWindow;
	Mat initMat;
	bool invisibleWrite;
	bool eraserWrite;
	vector<Mat> icons;
	deque<Mat> history;
	int historyIndex;

	void initIcons();
	void initModeBoard();
	void updateThicknessIcon();
	void updateModeIcon();

public:
	void init();
	void show();
	void changeThickness(string direction);
	void changeMode(Mode mode);
	void changeColor();
	void eraseAll();
	Mode getMode();
	void line(Point a, Point b);
	void enableInvisibleWrite();
	void disableInvisibleWrite();
	bool isWriteInvisible();
	void undo();
	void redo();
	void addToHistory();
	void eraser();
};
