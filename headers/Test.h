#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "StaticRecognition.h"
#include "DynamicRecognition.h"

using namespace std;
using namespace cv;

class Test {
private:
	StaticRecognition staticRecognition;
	double staticResult;

public:
	void testStaticRecognition(int finger);
	double getStaticResult();

};