#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#define X_SIZE 400
#define Y_SIZE 400

using namespace cv;

class BackgroundRemover {
private:
	Mat background;
	Mat foreground;
	Mat foregroundMask;
	Ptr<BackgroundSubtractor> backgroundSubtractor;

	void filterImage(Mat& img);


public:
	BackgroundRemover();
	void calibrate(Mat frame);
	Mat getForeground(Mat frame);
};