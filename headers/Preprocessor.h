#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

void morphologicalProcessing(Mat& frame);
int findBiggestContour(vector<vector<Point>> contours);

