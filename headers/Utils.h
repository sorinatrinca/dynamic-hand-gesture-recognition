#pragma once

#include <string>
#include <vector>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

void sortPoints(vector<Point>& points);
double distanceBetweenPoints(Point a, Point b);
void displayNumberOfFingers(int nr, Mat& frame);
int angleToCenter(Point finger, Point center);
void displayAngles(Mat& frame, vector<Point> fingers, vector<int> angles);
void displayStartMessage(Mat& frame);
int computeR(Point startPoint, Point endPoint);
void displayDynamicGesture(Mat& frame, string label);
double angleBetween3Points(Point a, Point b, Point c);