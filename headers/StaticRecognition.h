#pragma once
#include "DTW.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

class StaticRecognition {
private:
	Mat trainedData;
	Mat trainedClass;

	int numberOfFingers;
	Rect boundRect;

	double thinnesRatio;

	vector<Point> filterDefectsBasedOnJoints(Mat& frame, vector<Point>& innerPoints, Point centerPoint, vector<Vec4i> defects, vector<Point> contour);
	vector<Point> filterDefectsBasedOnFingerProperty(Mat& frame, vector<Point>& innerPoints, Point centerPoint, vector<Vec4i> defects, vector<Point> contour);
	vector<Vec4i> filterDefects(Mat& frame, vector<Point>& innerPoints, Point centerPoint, vector<Vec4i> defects, vector<Point> contour);
	
	Point findOneFinger(Point centerPoint, vector<Vec4i> defects, vector<Point> contour);

	vector<Vec4i> sortDefects(vector<Vec4i> defects, vector<Point> contour);

public:

	void recognizePosture(Mat& frame, vector<Point> contour, Point centerPoint);
	int getNumberOfFingers();
	double getThinnesRatio();
};