#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;


Mat computeDistanceTrajectory(vector<double> temp, vector<double> frame);
Mat computeDistancePostures(vector<double> temp, vector<double> frame);
double findBestPath(int type, vector<double> reference, vector<double> test);