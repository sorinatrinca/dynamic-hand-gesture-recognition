
#include "Test.h"
#include "Preprocessor.h"


void Test::testStaticRecognition(int finger)
{
	staticResult = 0;

	int samples = 500;
	vector<int> results = { 0,0,0,0,0,0 };

	/*ofstream myfile;
	myfile.open("test/thinnes/5.txt");*/

	for (int j = 1; j < samples+1; j++) {
		string path = "test/static/";
		string p = "/";
		path.append(to_string(finger));
		path.append(p);
		path.append(to_string(j));
		path.append(".jpg");

		Mat frame, hsvFrame, thresholdFrame;

		frame = imread(path);

		resize(frame, frame, Size(800, 800));
		flip(frame, frame, 1);

		cvtColor(frame, hsvFrame, COLOR_BGR2HSV);

		//inRange(hsvFrame, Scalar(104, 72, 0), Scalar(152, 255, 255), thresholdFrame);
		inRange(hsvFrame, Scalar(92, 53, 0), Scalar(146, 255, 255), thresholdFrame);

		medianBlur(thresholdFrame, thresholdFrame, 11);
		morphologicalProcessing(thresholdFrame);

		vector<vector<Point>> contours;
		vector<Vec4i> hierarchy;

		findContours(thresholdFrame, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE);

		if (contours.size() > 0) {

			int indexOfBiggestContour = findBiggestContour(contours);

			if (contourArea(contours[indexOfBiggestContour]) > 8000) {

				vector<Point> maxContour(contours[indexOfBiggestContour]);

				drawContours(frame, contours, indexOfBiggestContour, Scalar(0, 0, 255), 4, 8, hierarchy, 0);

				Moments moment = moments(maxContour, true); // or Mat(contours..)
				int cX = moment.m10 / moment.m00;
				int cY = moment.m01 / moment.m00;

				Point centerPoint(cX, cY);
				circle(frame, centerPoint, 3, Scalar(255, 0, 0), FILLED);

				staticRecognition.recognizePosture(frame, contours[indexOfBiggestContour], centerPoint);

				int noFingers = staticRecognition.getNumberOfFingers();

				results[noFingers]++;

				if (noFingers == finger) {
					staticResult++;
				}

				//myfile << staticRecognition.getThinnesRatio();
				//myfile << endl;

			}

		}
	}

	for (int r : results) {
		cout << r << " ";
	}
	cout << endl;
	staticResult = staticResult / samples;	
}

double Test::getStaticResult()
{
	return staticResult;
}

