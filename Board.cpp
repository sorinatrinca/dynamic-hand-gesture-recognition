#include "Board.h"

using namespace cv;
using namespace std;

void Board::init() {
	window = Mat(800, 800, CV_8UC3, Scalar(255,255,255));
	initMat = Mat(800, 800, CV_8UC3, Scalar(255, 255, 255));
	auxWindow = Mat(800, 800, CV_8UC3, Scalar(255, 255, 255));
	penThickness = medium;
	penColor = Scalar(0, 0, 0);
	currentColor = black;
	invisibleWrite = false;
	eraserWrite = false;
	initIcons();
	initModeBoard();

	history.push_back(window(Rect(0, 50, 800, 750)).clone());
	historyIndex = 0;
	
}

void Board::initIcons() {
	Mat pen = imread("images/icons/pen.png");
	Mat settings = imread("images/icons/settings.png");

	Mat bold = imread("images/icons/bold1.png");
	Mat medium = imread("images/icons/medium1.png");
	Mat fine = imread("images/icons/fine1.png");

	Mat black = imread("images/icons/black.png");
	Mat red = imread("images/icons/red.png");
	Mat blue = imread("images/icons/blue.png");
	Mat green = imread("images/icons/green.png");
	Mat magenta = imread("images/icons/magenta.png");
	Mat invisible = imread("images/icons/invisible.png");
	Mat white(50, 50, CV_8UC3, Scalar(255, 255, 255));
	Mat eraser = imread("images/icons/eraser.png");

	icons = { pen,settings, bold, medium, fine, black, red, blue, green, magenta, invisible, white, eraser };

	for (int i = 0; i < icons.size();i++) {
		Mat mat = icons[i];
		resize(mat, mat, Size(50, 50));
		icons[i] = mat;
	}
	
	icons[0].copyTo(window(Rect(10,0,50,50)));
	icons[1].copyTo(window(Rect(80, 0, 50, 50)));
	
	icons[3].copyTo(window(Rect(620, 0, 50, 50))); // thickness
	icons[5].copyTo(window(Rect(700, 0, 50, 50))); // color

	window.copyTo(initMat);
}


void Board::show() {
	if (invisibleWrite) {
		imshow("", auxWindow);
	}
	else{ imshow("", window); }
}

void Board::changeThickness(string direction) {
	if (direction.compare("down") == 0) {
		if (penThickness != ThicknessMode::bold) {
			if (penThickness == fine) { penThickness = medium; }
			else { penThickness = bold; }
		}
	}
	else if(direction.compare("up") == 0) {
		if (penThickness != ThicknessMode::fine) {
			if (penThickness == bold) { penThickness = medium; }
			else { penThickness = fine; }
		}
	}

	updateThicknessIcon();
}

void Board::updateThicknessIcon() {
	switch (penThickness) {
	case fine:
		icons[4].copyTo(window(Rect(620, 0, 50, 50))); break;
	case medium:
		icons[3].copyTo(window(Rect(620, 0, 50, 50))); break;
	case bold:
		icons[2].copyTo(window(Rect(620, 0, 50, 50))); break;
	}
}

void Board::initModeBoard() {
	userMode = writeMode;
	enableInvisibleWrite();

	Mat mask, src1 = icons[0];
	inRange(src1, Scalar(255, 255, 255), Scalar(255, 255, 255), mask);
	src1.setTo(Scalar(222, 213, 171), mask);
	icons[0] = src1;
	icons[0].copyTo(window(Rect(10, 0, 50, 50)));
}

void Board::changeMode(Mode mode)
{
	if (mode != userMode) {
		userMode = mode;
		updateModeIcon();
	}
}

void Board::updateModeIcon() {
	Mat src1, src2, mask;
	int index1, index2;
	Point2i p1, p2;

	if (userMode == Mode::writeMode) {
		src1 = icons[0]; src2 = icons[1]; index1 = 0; index2 = 1; p1 = Point2i(10, 0); p2 = Point2i(80, 0);
	}
	else {
		src2 = icons[0]; src1 = icons[1]; index1 = 1; index2 = 0; p2 = Point2i(10, 0); p1 = Point2i(80, 0);
	}

	inRange(src1, Scalar(255, 255, 255), Scalar(255, 255, 255), mask);
	src1.setTo(Scalar(222, 213, 171), mask);
	icons[index1] = src1;
	icons[index1].copyTo(window(Rect(p1.x, p1.y, 50, 50)));

	inRange(src2, Scalar(222, 213, 171), Scalar(222, 213, 171), mask);
	src2.setTo(Scalar(255, 255, 255), mask);
	icons[index2] = src2;
	icons[index2].copyTo(window(Rect(p2.x, p2.y, 50, 50)));
}

void Board::changeColor()
{
	switch (currentColor) {
		case black:
			penColor = Scalar(0, 0, 255); currentColor = red;  break;
		case red:
			penColor = Scalar(255, 0, 0); currentColor = blue;  break;
		case blue:
			penColor = Scalar(0, 128, 0); currentColor = green; break;
		case green:
			penColor = Scalar(255, 0, 255); currentColor = magenta; break;
		case magenta:
			penColor = Scalar(0, 0, 0); currentColor = black; break;
		default:
			penColor = Scalar(0, 0, 0); currentColor = black;
	}

	// change icon
	icons[currentColor+5].copyTo(window(Rect(700, 0, 50, 50)));
}

void Board::eraseAll()
{
	initMat(Rect(0, 50, 800, 750)).copyTo(window(Rect(0, 50, 800, 750)));
	addToHistory();
	//updateModeIcon();
	//updateThicknessIcon();
}

Mode Board::getMode()
{
	return userMode;
}

void Board::line(Point a, Point b) {

	Point a1 = Point( a.x, a.y);
	Point b1 = Point( b.x, b.y);

	if (userMode == writeMode) {
		if (invisibleWrite) {
			cv::line(auxWindow(Range(50,799),Range(0,799)), a1, b1, Scalar(0,255,255), penThickness, LINE_AA);
		}
		else if (eraserWrite) {
			cv::line(window(Range(50, 799), Range(0, 799)), a1, b1, Scalar(255,255,255), penThickness, LINE_AA);
		}
		else {
			cv::line(window(Range(50, 799), Range(0, 799)), a1, b1, penColor, penThickness, LINE_AA);
		}
	}
}

void Board::enableInvisibleWrite()
{
	invisibleWrite = true;
	window.copyTo(auxWindow);
	icons[10].copyTo(window(Rect(540, 0, 50, 50)));
}

void Board::disableInvisibleWrite()
{
	invisibleWrite = false;
	icons[11].copyTo(window(Rect(540, 0, 50, 50)));
}

bool Board::isWriteInvisible()
{
	return invisibleWrite;
}

void Board::undo()
{
	if (historyIndex > 0) {
		history[--historyIndex].copyTo(window(Rect(0, 50, 800, 750)));
	}
}

void Board::redo()
{
	if (historyIndex < (history.size()-1)) {
		history[++historyIndex].copyTo(window(Rect(0, 50, 800, 750)));
	}
}

void Board::addToHistory()
{
	history.push_back(window(Rect(0, 50, 800, 750)).clone());
	historyIndex++;
}

void Board::eraser() {
	eraserWrite = !eraserWrite;
	if (eraserWrite) {
		icons[12].copyTo(window(Rect(150, 0, 50, 50)));
	}
	else {
		icons[11].copyTo(window(Rect(150, 0, 50, 50)));
	}
}
