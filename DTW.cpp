#include "DTW.h"


int min3(int a, int b, int c) {
	return min(a, min(b, c));
}

Mat computeDistancePostures(vector<double> reference, vector<double> test) {

	int horizontal = test.size();
	int vertical = reference.size();

	Mat distance = Mat(vertical, horizontal, CV_64FC1);

	for (int i = 0; i < vertical; i++) {
		for (int j = 0; j < horizontal; j++) {
			double d = 0;
			if (i > 0 && j > 0) {
				double d1 = distance.at<double>(i - 1, j - 1);
				double d2 = distance.at<double>(i - 1, j);
				double d3 = distance.at<double>(i, j - 1);
				
				d = abs(reference[i] - test[j]) + min3(d1, d2, d3);
				distance.at<double>(i, j) = d;
			}
			else if (i > 0) {
				double d2 = distance.at<double>(i - 1, j);
	
				d = abs(reference[i] - test[j]) + d2;
				distance.at<double>(i, j) = d;
			}
			else if (j > 0) {
				double d3 = distance.at<double>(i, j - 1);

				d = abs(reference[i] - test[j]) + d3;
				distance.at<double>(i, j) = d;
			}
			else {
				d = abs(reference[i] - test[j]);
				distance.at<double>(i, j) = d;
			}
		}
	}

	return distance;
}

Mat computeDistanceTrajectory(vector<double> reference, vector<double> test) {
	int horizontal = test.size();
	int vertical = reference.size();

	Mat distance = Mat(vertical, horizontal, CV_64FC1);

	for (int i = 0; i < vertical; i++) {
		for (int j = 0; j < horizontal; j++) {
			double d = 0;

			double absDistance = abs(reference[i] - test[j]);
			if (absDistance <= 8) {
				d = absDistance;
			}
			else {
				d = 16 - absDistance;
			}

			if (i > 0 && j > 0) {
				double d1 = distance.at<double>(i - 1, j - 1);
				double d2 = distance.at<double>(i - 1, j);
				double d3 = distance.at<double>(i, j - 1);
				
				d+= min3(d1, d2, d3);
				distance.at<double>(i, j) = d;
			}
			else if (i > 0) {
				double d2 = distance.at<double>(i - 1, j);

				d += d2;
				distance.at<double>(i, j) = d;
			}
			else if (j > 0) {
				double d3 = distance.at<double>(i, j - 1);

				d += d3;
				distance.at<double>(i, j) = d;
			}
			else {
				distance.at<double>(i, j) = d;
			}
		}
	}

	return distance;
}

// type = 0 for trajectories and type = 1 for postures
double findBestPath(int type, vector<double> reference, vector<double> test) {
	Mat distance;

	if (type == 0) {
		distance = computeDistanceTrajectory(reference, test);
	}
	else {
		distance = computeDistancePostures(reference, test);
	}

	vector<Point3d> path;

	int i = distance.rows - 1;
	int j = distance.cols - 1;
	int x = distance.at<double>(i, j);
	path.push_back(Point3d(i,j, distance.at<double>(i, j)) );

	int poz = 1;
	bool finish = false;

	while (!finish) {
		if (i > 0 && j > 0) {
			int d1 = distance.at<double>(i, j - 1);
			int d2 = distance.at<double>(i - 1, j);
			int d3 = distance.at<double>(i - 1, j - 1);

			int minD = min3(d1, d2, d3);
			if (minD == d1) {
				path.push_back(Point3i(i, j - 1, minD));
				j--;
			}
			else if (minD == d2) {
				path.push_back(Point3i(i - 1, j, minD));
				i--;
			}
			else {
				path.push_back(Point3i(i - 1, j - 1, minD));
				i--;
				j--;
			}
		}
		else if (i == 0) {
			int d1 = distance.at<double>(i, j - 1);
			path.push_back(Point3i(i, j - 1, d1));
			j--;
		}
		else {
			int d2 = distance.at<double>(i - 1, j);
			path.push_back(Point3i(i - 1, j, d2));
			i--;
		}

		if (path[poz].x == 0 && path[poz].y == 0)
		{
			finish = true;
		}

		poz++;

	}

	double value = 0;

	for (int i = 0; i < path.size(); i++) {
		value += path[i].z;
	}
	return value;
}

