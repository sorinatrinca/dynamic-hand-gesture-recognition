#include "Utils.h"

#define PI 3.14159265
#define X_SIZE 600
#define Y_SIZE 600

void sortPoints(vector<Point>& points) {
	for (int i = 0; i < points.size() - 1; i++) {
		for (int j = i + 1; j < points.size(); j++) {
			if (points[i].x > points[j].x) {
				Point aux = points[i];
				points[i] = points[j];
				points[j] = aux;
			}
		}

	}
}

double distanceBetweenPoints(Point a, Point b) {
	double x = (a.x - b.x) * (a.x - b.x);
	double y = (a.y - b.y) * (a.y - b.y);

	return sqrt(x + y);
}

// Calculate the angle between two points
int angleToCenter(Point finger, Point center) {
	double deltaX = finger.x - center.x;
	double deltaY = center.y - finger.y;

	double theta_radians = atan2(deltaY, deltaX);

	int degrees = (int)round(theta_radians * 180 / PI);
	degrees = (degrees + 360) % 360;
	return degrees;
}

double angleBetween3Points(Point a, Point b, Point c) {
	double ab = distanceBetweenPoints(a, b);
	double ac = distanceBetweenPoints(a, c);
	double bc = distanceBetweenPoints(b, c);

	return acos((ab * ab + bc * bc - ac * ac) / (2 * ab * bc)) * 180 / CV_PI;
}

void displayNumberOfFingers(int nr, Mat& frame) {

	char msg[50]="";
	bool ok = false;

	if (nr == 1) {
		strcpy_s(msg, "ONE");
		ok = true;
	}
	else if (nr == 2) {
		strcpy_s(msg, "TWO");
		ok = true;
	}
	else if (nr == 3) {
		strcpy_s(msg, "THREE");
		ok = true;
	}
	else if (nr == 4) {
		strcpy_s(msg, "FOUR");
		ok = true;
	}
	else if (nr == 5) {
		strcpy_s(msg, "FIVE");
		ok = true;
	}
	else if (nr == 0) {
		strcpy_s(msg, "ZERO");
		ok = true;
	}

	if (ok)
		putText(frame, msg, Point(50, 50), FONT_HERSHEY_SIMPLEX, 1, Scalar(255, 0, 0), 2, 2, false);
}

void displayAngles(Mat& frame, vector<Point> fingers, vector<int> angles) {
	string msg;

	for (int i = 0; i < fingers.size(); i++) {
		if (angles[i] > 0)
		{
			msg = to_string(i + 1);
			putText(frame, msg, Point(fingers[i].x, fingers[i].y - 20), FONT_HERSHEY_DUPLEX, 0.5, Scalar(255, 130, 10), 2, 2, false);
		}
	}
}

void displayStartMessage(Mat& frame) {
	char msg[50];
	strcpy_s(msg, "START!");
	putText(frame, msg, Point(100, 100), FONT_HERSHEY_SIMPLEX, 1, Scalar(0, 255, 0), 2, 2, false);
}

void displayDynamicGesture(Mat& frame, string label) {
	putText(frame, label, Point(150, 150), FONT_HERSHEY_SIMPLEX, 1, Scalar(0, 0, 255), 2, 2, false);
}

int computeR(Point startPoint, Point endPoint) {
	int angle = angleToCenter(startPoint, endPoint);
	int r = 0;

	if (angle >= 0 && angle < 22.5) {
		r = 0;
	}
	else if (angle >= 22.5 && angle < 45) {
		r = 1;
	}
	else if (angle >= 45 && angle < 67.5) {
		r = 2;
	}
	else if (angle >= 67.5 && angle < 90) {
		r = 3;
	}
	else if (angle >= 90 && angle < 112.5) {
		r = 4;
	}
	else if (angle >= 112.5 && angle < 135) {
		r = 5;
	}
	else if (angle >= 135 && angle < 157.5) {
		r = 6;
	}
	else if (angle >= 157.5 && angle < 180) {
		r = 7;
	}
	else if (angle >= 180 && angle < 202.5) {
		r = 8;
	}
	else if (angle >= 202.5 && angle < 225) {
		r = 9;
	}
	else if (angle >= 225 && angle < 247.5) {
		r = 10;
	}
	else if (angle >= 247.5 && angle < 270) {
		r = 11;
	}
	else if (angle >= 270 && angle < 292.5) {
		r = 12;
	}
	else if (angle >= 292.5 && angle < 315) {
		r = 13;
	}
	else if (angle >= 315 && angle < 337.5) {
		r = 14;
	}
	else if (angle >= 337.5 && angle < 360) {
		r = 15;
	}

	return r;
}

