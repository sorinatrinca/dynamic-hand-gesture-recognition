#include "StaticRecognition.h"
#include "Preprocessor.h"
#include "Utils.h"
#include <fstream>

#define X_SIZE 200
#define Y_SIZE 200

#define BOUNDING_RECT_FINGER_SIZE_SCALING 0.22
#define BOUNDING_RECT_NEIGHBOR_DISTANCE_SCALING 0.14 //0.15
#define BOUNDING_RECT_NEIGHBOR_DISTANCE_X_SCALING 0.14
#define FINGER_LENGTH_SCALING 0.4

struct comparePointsSet {
	bool operator()(Point a, Point b) const {
		return (a.x < b.x && a.y < b.y);
	}
};

bool comparePoints(Point p1, Point p2) {
	return p1.x < p2.x;
}


vector<Vec4i> StaticRecognition::sortDefects(vector<Vec4i> defects, vector<Point> contour) {

	int index =0;
	int maxStartOnX = DBL_MIN;

	for (int i = 0; i < defects.size(); i++) {
		int p_start = defects[i][0];

		if (contour[p_start].x > maxStartOnX) {
			maxStartOnX = contour[p_start].x;
			index = i;
		}
	}

	vector<Vec4i> result(defects.begin() + index, defects.end());

	for (int i = 0; i < index; i++)
	{
		result.push_back(defects[i]);
	}

	return result;
}


vector<Point> StaticRecognition::filterDefectsBasedOnJoints(Mat &frame,vector<Point>& innerPoints, Point centerPoint, vector<Vec4i> defects, vector<Point> contour) {
	vector<Point> filteredDefects;

	bool first = true;

	for (int k = 0; k < defects.size(); k++) {
		if (defects[k][3] > 500) {
			int p_start = defects[k][0];
			int p_end = defects[k][1];
			int p_far = defects[k][2];

			if (first) {
				if (centerPoint.y >= contour[p_start].y && centerPoint.y >= contour[p_end].y) {
					if (angleBetween3Points(contour[p_start], contour[p_far], contour[p_end]) < 100) {
						filteredDefects.push_back(contour[p_start]);
						filteredDefects.push_back(contour[p_end]);
						first = false;
					}

				}
			}
			else {
				if (centerPoint.y >= contour[p_start].y && centerPoint.y >= contour[p_end].y) {
					if (angleBetween3Points(contour[p_start], contour[p_far], contour[p_end]) < 100) {
						filteredDefects.push_back(contour[p_end]);
					}
				}
			}

		}
		
	}

	return vector<Point>(filteredDefects.begin(), filteredDefects.end());
}


vector<Vec4i> StaticRecognition::filterDefects(Mat& frame, vector<Point>& innerPoints, Point centerPoint, vector<Vec4i> defects, vector<Point> contour) {
	vector<Vec4i> filteredDefects;

	for (int k = 0; k < defects.size(); k++) {
		if (defects[k][3] > 2000) {
			int p_start = defects[k][0];
			int p_end = defects[k][1];
			int p_far = defects[k][2];

			if (centerPoint.y >= contour[p_start].y && centerPoint.y >= contour[p_end].y) {
				filteredDefects.push_back(defects[k]);
			}
		}
	}

	return filteredDefects;
}

vector<Point> StaticRecognition::filterDefectsBasedOnFingerProperty(Mat& frame, vector<Point>& innerPoints, Point centerPoint, vector<Vec4i> defects, vector<Point> contour) {
	vector<Point> filteredDefects;

	bool first = true;

	Point firstPoint, firstDefect;
	Point lastDefect;

	for (int k = 0; k < defects.size(); k++) {
		if (defects[k][3] > 500) {
			int p_start = defects[k][0];
			int p_end = defects[k][1];
			int p_far = defects[k][2];

			if (first) {
				if (centerPoint.y >= contour[p_start].y) {
					firstPoint = contour[p_start];
					firstDefect = contour[p_far];
					first = false;

					lastDefect = firstDefect;
				}
			}
			else {
				if (centerPoint.y >= contour[p_start].y) {
					if (angleBetween3Points(lastDefect, contour[p_start], contour[p_far]) < 45) {
						filteredDefects.push_back(contour[p_start]);
					}
				}

				lastDefect = contour[p_far];
			}

		}

	}

	if (angleBetween3Points(lastDefect, firstPoint, firstDefect) < 45) {
		filteredDefects.push_back(firstPoint);
	}

	return vector<Point>(filteredDefects.begin(), filteredDefects.end());
}

int StaticRecognition::getNumberOfFingers() {
	return numberOfFingers;
}

double StaticRecognition::getThinnesRatio()
{
	return thinnesRatio;
}

Point StaticRecognition::findOneFinger(Point centerPoint, vector<Vec4i> defects, vector<Point> contour) {
	
	double maxDistance = DBL_MIN;
	Point maxPoint;

	for (int k = 0; k < defects.size(); k++) {
		
			int p_start = defects[k][0];
			int p_end = defects[k][1];
			int p_far = defects[k][2];

			if (centerPoint.y >= contour[p_start].y ) {
				double distance = abs(centerPoint.y - contour[p_start].y);
				if (distance > maxDistance) {
					maxDistance = distance;
					maxPoint = contour[p_start];
				}
			}



	}

	return maxPoint;
}

void StaticRecognition::recognizePosture(Mat& frame, vector<Point> contour, Point centerPoint) {

	vector<int> hull;
	vector<Point> filteredDefects;
	vector<Point> innerPoints;

	// find thinness ratio
	double perimeter = arcLength(contour, true);
	double area = contourArea(contour);
	thinnesRatio = (4 * CV_PI * area) / (perimeter * perimeter);

	// Find the convex hull
	convexHull(Mat(contour), hull, true);

	// Convexity Defects - needs more than 3 hull points
	vector<Vec4i> defects;
	convexityDefects(contour, hull, defects);

	defects = sortDefects(defects, contour);

	// Method 1
	//filteredDefects = filterDefectsBasedOnFingerProperty(frame, innerPoints, centerPoint, defects, contour);

	//Method 2
	filteredDefects = filterDefectsBasedOnJoints(frame, innerPoints, centerPoint, defects, contour);

	// if thinnes ratio is too small for the contour to be without fingers, we are in case 3: one finger
	if (filteredDefects.empty() && thinnesRatio < 0.44){
		filteredDefects.push_back(findOneFinger(centerPoint,defects,contour));
	}

	// case when ratio is too big to have any fingers
	if (thinnesRatio > 0.44) {
		filteredDefects.clear();
	}

	for (int i = 0; i < filteredDefects.size(); i++) {
		circle(frame, filteredDefects[i], 3, Scalar(0, 0, 0), 2);
		cv::line(frame, centerPoint, filteredDefects[i], Scalar(0, 255, 0), 3, LINE_AA, 0);
	}

	numberOfFingers = filteredDefects.size();
}


