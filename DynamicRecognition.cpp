#include "DynamicRecognition.h"
#include "DTW.h"
#include "Utils.h"

#define NO_TEMPLATES 4
#define LINE_LENGTH 10
#define TRJ_THRESHOLD 70
#define GST_THRESHOLD 80

#define SAMPLES_PER_SET 10

vector<string> dynamicGestures = { "up", "down", "circle", "square", "left", "right", "hello", "semi-left","semi-right" };

// Current trajectory
vector<int> trajectory;

// Last trafectories
vector<int> lastTrajectories;

void DynamicRecognition::init() {	
	int matRows = dynamicGestures.size() * SAMPLES_PER_SET;
	directionTemplates = Mat(matRows, 50, CV_32SC1);
	staticGesturesTemplates = Mat(matRows, 50, CV_32SC1);
	int rowX = 0;

	for (string gesture : dynamicGestures) {
		string path = "dynamic/";
		path.append(gesture);
		path.append(".txt");

		ifstream templatesFile(path);

		if (templatesFile.is_open()) {
			string line;
			vector<int> directions;
			vector<int> gestures;

			while (getline(templatesFile, line)) {
				directions.clear();
				istringstream iss(line);
				int dir, gest;
				char c;
				while (iss >> c >> dir >> c >> gest >> c) {
					directions.push_back(dir);
					gestures.push_back(gest);
				}
				int n = directions.size();

				directionTemplates.at<int>(rowX, 0) = n;
				staticGesturesTemplates.at<int>(rowX, 0) = n;

				for (int i = 0; i < n; i++) {
					directionTemplates.at<int>(rowX, i + 1) = directions[i];
					staticGesturesTemplates.at<int>(rowX, i + 1) = gestures[i];
				}

				labels.push_back(gesture);
				rowX++;
			}
		}

		templatesFile.close();
	}

	cout << directionTemplates.rows << " " << staticGesturesTemplates.rows << " " << labels.size();
}

void DynamicRecognition::endCurrentGesture() {
	lastTrajectories.clear();
}


bool oneStaticGesture(vector<double> gestures) {
	bool status = true;
	if (gestures.size() > 0) {
		double f = gestures[0];

		for (double g : gestures) {
			if (f!=g) {
				status = false;
				break;
			}
		}
	}

	return status;
}

string DynamicRecognition::recognize(Mat& frame, vector<double> relativePoints, vector<double> staticGestures)
{
	double minDirection = DBL_MAX, minGesture = DBL_MAX;
	string directionLabel = "", staticGestureLabel = "";

	// trajectory 

	vector<double> tCosts;
	vector<double> tDivCosts;
	int k = 0;
	double sum = 0;

	int trajIndex = 0;


	for (int i = 0; i < directionTemplates.rows; i++) {
		k++;

		int rowSize = directionTemplates.at<int>(i, 0);

		vector<double> v1 = directionTemplates.row(i);
		vector<double> row = vector<double>(v1.begin() + 1, v1.begin() + rowSize + 1);

		double value = findBestPath(0,relativePoints, row);

		tCosts.push_back(value);
		sum += value;

		if (k % SAMPLES_PER_SET == 0) {
			tDivCosts.push_back(sum);
			k = 0;
			sum = 0;
		}

		if (value < minDirection) {
			minDirection = value;
			directionLabel = labels.at(i);
			trajIndex = i;
		}
	}

	double minT = *min_element(tDivCosts.begin(), tDivCosts.end());
	int minPosT = distance(tDivCosts.begin(), min_element(tDivCosts.begin(), tDivCosts.end()));

	// static gestures

	vector<double> sCosts;
	vector<double> sDivCosts;
	vector<double> sRow, sRowTrajectory;
	k = 0;
	sum = 0;

	for (int i = 0; i < staticGesturesTemplates.rows; i++) {
		k++;

		int rowSize = staticGesturesTemplates.at<int>(i, 0);

		vector<double> v1 = staticGesturesTemplates.row(i);
		vector<double> row = vector<double>(v1.begin() + 1, v1.begin() + rowSize + 1);

		// posture of the found trajectory
		if (i == trajIndex) {
			sRowTrajectory = row;
		}

		double value = findBestPath(1,staticGestures, row);
		sCosts.push_back(value);
		sum += value;

		// calculez suma tuturor rezultatelor pentru setul X de gesturi
		if (k % SAMPLES_PER_SET == 0) {
			sDivCosts.push_back(sum);
			k = 0;
			sum = 0;
		}

		if (value < minGesture) {
			minGesture = value;
			staticGestureLabel = labels.at(i);
			sRow = row;
		}
	}

	// find min sum
	double minS = *min_element(sDivCosts.begin(), sDivCosts.end());
	// index of min sum
	int minPosS = distance(sDivCosts.begin(), min_element(sDivCosts.begin(), sDivCosts.end()));

	// only one static gesture per dynamic gesture -> min 70 %

	bool ok = false;
	bool sameStatiGesture = false;

	// if founded posture sequence have only 1 static gesture
	if (oneStaticGesture(sRow)) {
		int count = 0;
		for (double g : staticGestures) {
			if (g == sRow[0]) {
				count++;
			}
		}
		if ((float)count / (float)sRow.size() > 0.7) {
			ok = true;
		}
		if (oneStaticGesture(sRowTrajectory)) {
			if (sRowTrajectory[0] == sRow[0]) {
				sameStatiGesture = true;
			}
		}

		if (ok && !oneStaticGesture(sRowTrajectory))
			ok = false;
	}
	else { ok = true; }

	if (ok) {
		if (minPosT == minPosS || staticGestureLabel.compare(directionLabel) == 0 || sameStatiGesture) {
			displayDynamicGesture(frame, directionLabel);
			return directionLabel;
		}
		else if (sDivCosts[trajIndex / SAMPLES_PER_SET] == minS) { // Mai adaug inversa?
			displayDynamicGesture(frame, directionLabel);
			return directionLabel;
		}
	}

	return "";

}



