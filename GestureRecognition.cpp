// GestureRecognition.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <deque>
#include <queue>
#include <chrono>

#include "DTW.h"
#include "DynamicRecognition.h"
#include "StaticRecognition.h"
#include "Preprocessor.h"
#include "Utils.h"
#include "Board.h"
#include "BackgroundRemover.h"
#include "Test.h"
#include "HandDetector.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <numeric>

#define PI 3.14159265

// Define the line length (max number of tracking points at a time)
#define LINE_LENGTH 5
#define MIN_TRAJECTORY_LENGTH 5
#define FPS 30
#define X_SIZE 800
#define Y_SIZE 800

using namespace cv;
using namespace std;

DynamicRecognition dynamicRecognition;
StaticRecognition staticRecognition;
Board board;
BackgroundRemover backgroundRemover;
Test test;
HandDetector handDetector;

// Global queues for hand tracking
deque<Point> trackingPoints;
deque<Point> nothingModePoints;
deque<Point> writeModePoints;

deque<int> relativeTrackingValue;
deque<int> staticGestures;

auto startTime = chrono::steady_clock::now(), endTime = chrono::steady_clock::now();


void computeWritingPoints(Point centerPoint) {

	int noFingers = staticRecognition.getNumberOfFingers();

	if (board.isWriteInvisible() && noFingers == 0) {
		nothingModePoints.push_back(centerPoint);

		int n = nothingModePoints.size();
		if (n > 1) {
			board.line(nothingModePoints[n - 2], nothingModePoints[n - 1]);
		}

		writeModePoints.clear();
	}
	else if (board.isWriteInvisible() == false && noFingers == 1) {
		writeModePoints.push_back(centerPoint);

		int n = writeModePoints.size();
		if (n > 1) {
			board.line(writeModePoints[n - 2], writeModePoints[n - 1]);
		}

		nothingModePoints.clear();
	}
}

void analyzeDynamicGesture(string gesture) {

	Mode currentBoardMode = board.getMode();

	// this is for changing board mode
	if (gesture.compare("right") == 0) {
		if (currentBoardMode == Mode::writeMode) {
			board.changeMode(Mode::settingMode);
			board.disableInvisibleWrite();

			// history update if necessary
			if (writeModePoints.size() > 0) {
				board.addToHistory();
			}
		}
	}
	else if (gesture.compare("left") == 0) {
		if (currentBoardMode == Mode::settingMode) {
			board.changeMode(Mode::writeMode);
		}
	}
	else {
		// this is only for setting mode
		if (currentBoardMode == Mode::settingMode) {
			if (gesture.compare("up") == 0) {
				board.changeThickness("up");
			}
			else if (gesture.compare("down") == 0) {
				board.changeThickness("down");
			}
			else if (gesture.compare("circle") == 0) {
				board.eraseAll();
			}
			else if (gesture.compare("semi-left") == 0) {
				board.undo();
			}
			else if (gesture.compare("semi-right") == 0) {
				board.redo();
			}
			else if (gesture.compare("square") == 0) {
				board.changeColor();
			}
			else if (gesture.compare("hello") == 0) {
				board.eraser();
			}
		}
	}
}


void addToTrajectory(Mat& frame, Point centerPoint) {

	bool start = false;

	Mode boardMode = board.getMode();

	if (boardMode == writeMode) {
		computeWritingPoints(centerPoint);
	}

	if (trackingPoints.size() >= 1) {

		Point startPoint = trackingPoints.front();
		Point endPoint = centerPoint;

		if (distanceBetweenPoints(startPoint, endPoint) < 2) {

			if (relativeTrackingValue.size() >= MIN_TRAJECTORY_LENGTH) {
				string gesture = dynamicRecognition.recognize(frame, vector<double>(relativeTrackingValue.begin(), relativeTrackingValue.end()), vector<double>(staticGestures.begin(), staticGestures.end()));
				
				analyzeDynamicGesture(gesture);

			}

			// delete all tracking points 
			trackingPoints.clear();
			relativeTrackingValue.clear();
			staticGestures.clear();

			start = true;

			// display message
			displayStartMessage(frame);
			displayNumberOfFingers(staticRecognition.getNumberOfFingers(), frame);
		}
		else {
			// Compute and add relative value r to deque
			int r = computeR(endPoint, startPoint); //inversed because of indexing
			relativeTrackingValue.push_back(r);
			staticGestures.push_back(staticRecognition.getNumberOfFingers());
		}
	}

	trackingPoints.push_front(centerPoint);
}

void drawHandTrajectory(Mat& frame) {

	// If we have more than two tracking points
	if (trackingPoints.size() >= 2) {

		// Draw
		for (int i = 1; i < trackingPoints.size() - 1; i++) {

			// Compute the thickness
			int thickness = sqrt(LINE_LENGTH / float(i + 1) * 2.5)+1;

			line(frame, trackingPoints[i - 1], trackingPoints[i], Scalar(255, 255, 255), thickness);
		}

	}
}


void frameProcessing(Mat& thresholdFrame, Mat& frame) {
	// Contours
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;

	findContours(thresholdFrame, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE);

	if (contours.size() > 0) {
		// Find biggest contour
		int indexOfBiggestContour = findBiggestContour(contours);

		// If contour is big enough to represent a hand
		if (contourArea(contours[indexOfBiggestContour]) > 8000) {

			// Copy the biggest contour before aproximate it
			vector<Point> maxContour(contours[indexOfBiggestContour]);

			// aproximate the polygon that represent the contour with another polygon with less vertices, with some precision
			//approxPolyDP(contours[indexOfBiggestContour], contours[indexOfBiggestContour], 5, true);

			// Draw biggest contour
			drawContours(frame, contours, indexOfBiggestContour, Scalar(0, 0, 255), 4, 8, hierarchy, 0);

			// Find the center of contour
			Moments moment = moments(maxContour, true); // or Mat(contours..)
			int cX = moment.m10 / moment.m00;
			int cY = moment.m01 / moment.m00;

			Point centerPoint(cX, cY);
			circle(frame, centerPoint, 3, Scalar(255, 0, 0), FILLED);

			staticRecognition.recognizePosture(frame, contours[indexOfBiggestContour],centerPoint);

			int noFingers = staticRecognition.getNumberOfFingers();

			int boardMode = board.getMode();

			if (boardMode == writeMode) {
			if (noFingers == 0) {
					board.enableInvisibleWrite();
					if (writeModePoints.size() > 0) {
						board.addToHistory();
					}
				}
				else if (noFingers == 1) {
					board.disableInvisibleWrite();
				}
			}

			// Add to tracking queue a new point
			addToTrajectory(frame, centerPoint);
						
		}

	}
}

void handRecognition() {
	// Create video capturing object
	VideoCapture video(2);

	// For saving the frame
	Mat frame;
	Mat thresholdFrame;
	Mat auxFrame;

	video.set(CAP_PROP_FPS, 60);
	video.set(CAP_PROP_FRAME_WIDTH, 900);
	video.set(CAP_PROP_FRAME_HEIGHT, 900);

	// Check that video is opened
	if (!video.isOpened()) {
		cout << endl<< "cannot open camera"<<endl;
		return;
	}

	// Loop through available frames
	while (video.read(frame)) {
		startTime = chrono::steady_clock::now();
		// Resize frame before processing
		resize(frame, frame, Size(800, 800));

		// Flip frame
		flip(frame, frame, 1);
		frame.copyTo(auxFrame);

		Mat foreground = backgroundRemover.getForeground(frame);

		thresholdFrame = handDetector.detect(foreground);

		// Blur frame to remove basic imperfections
		medianBlur(thresholdFrame, thresholdFrame, 11);

		// Morphological processing
		morphologicalProcessing(thresholdFrame);

		// Detect and then track the hand
		frameProcessing(thresholdFrame, frame);
		drawHandTrajectory(frame);

		// Show information
		board.show();

		resize(frame, frame, Size(600, 600));

		//resize(frame, frame, Size(400, 400));
		imshow("Video feed", frame);

		int key = waitKey(1);

		if (key == 27) // esc
			break;
		else if (key == 98) // b
			backgroundRemover.calibrate(frame); 
	}

	// Release video capture
	video.release();

	// Destroy all windows
	cv::destroyAllWindows();
}

int main()
{
	board.init();
	dynamicRecognition.init();
	handDetector.init(92,146,53,255);
	handRecognition();

	//test.testStaticRecognition(5);
	//cout << test.getStaticResult();

}