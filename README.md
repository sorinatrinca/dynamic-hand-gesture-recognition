# Interactive whiteboard manipulation system using dynamic hand gestures recognition

The system involves a real-time interaction application with a smartboard, using dynamic hand gestures. The interaction model consists in making gestures and interpreting them as actions of manipulating the visual characteristics of the intuitive writing tool.
The application was developed using C++ and OpenCV and the modular structure of the system allows the addition or modification of current functionalities.

## Preview

![Desktopt](https://bitbucket.org/sorinatrinca/dynamic-hand-gesture-recognition/raw/645eaa5a34d014322ce6aab3794ab44f6b8126dc/Images/preview/Gestures.jpg)

## Features

- Detection of the human hand
- Identify the number of fingers raised associated with the hand
- Trajectory Composition
- Classification of dynamic gestures using Dynamic Time Warping Algorithm
- Generating commands that will operate the interactive whiteboard
- Modify the interactive whiteboard according to the user's gesture

## Architecture

![General Schema](https://bitbucket.org/sorinatrinca/dynamic-hand-gesture-recognition/raw/645eaa5a34d014322ce6aab3794ab44f6b8126dc/Images/preview/Architecture.jpg)
